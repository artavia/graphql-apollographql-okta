// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

// =============================================
// Boilerplate
// =============================================
const { ApolloServer, gql } = require("apollo-server");
const { AuthorizationError } = require("apollo-server");
const { v4: uuidv4 } = require("uuid");

// =============================================
// Import okta functionality
// =============================================
const { getToken, getUserIdFromToken, getUser } = require("./okta");

// =============================================
// schema declarations
// Apparently, block quotes """ DO jive
// =============================================
const typeDefs = gql`

  # An exclamation point (!) after a declared field's type means "this field's value can never be null."

  # E.G. - If an array has an exclamation point after it, an array cannot be null but it can be empty.

  """
  OBJECT TYPES  
  """
  type Quote {
    id: ID!
    phrase: String!
    attribution: String
  }

  type DeleteResponse {
    ok: Boolean!
  }

  type Authentication {
    token: String!
  }

  """
  QUERY TYPES - Queries enable clients to fetch data but not to modify data. 
  """
  type Query{
    # initial test
    hello: String
    # ...the rest
    quotes: [Quote]
  }

  # MUTATION TYPES - To enable clients to modify data, a schema needs to define some mutations.

  type Mutation {
    addQuote( phrase: String!, attribution: String ): Quote
    editQuote(id: ID!, phrase: String, attribution: String ): Quote
    deleteQuote( id: ID! ): DeleteResponse
    login(username: String!, password: String!): Authentication
  }
`;

// =============================================
// Dummy Data Setup
// =============================================
const quotes = {};
const addQuote = quote => {
  const id = uuidv4();
  return quotes[id] = { ...quote, id };
};

addQuote( { phrase: "Hehehehe! Molestias itaque ullam dolor nisi!" , attribution: "Luffy" } );
addQuote( { phrase: "Grrrr! Ratione molestias ducimus." , attribution: "Zoro" } );
addQuote( { phrase: "Kura-aaa! Quae exercitationem blanditiis repellendus odit." , attribution: "Nami" } );
addQuote( { phrase: "Fushi! Molestiae ipsam laudantium ab rem distinctio vero!" , attribution: "Usopp" } );
addQuote( { phrase: "Nami chwan! Distinctio molestias voluptatum quae assumenda." , attribution: "Sanji" } );
addQuote( { phrase: "Taskaze! Sapiente, id ratione cumque ducimus vero quis." , attribution: "Chopper" } );
addQuote( { phrase: "Tehehehe! Nesciunt veritatis aliquam sint hic." , attribution: "Robin" } );
addQuote( { phrase: "SUPER! Veritatis, nam! Illum quasi pariatur possimus natus porro." , attribution: "Franky" } );
addQuote( { phrase: "Yo ho ho ho! Cum, eos veritatis cumque natus minima!" , attribution: "Brook" } );

// =============================================
// GraphQL Endpoints
// =============================================
const resolvers = {
  Query: {
    hello: () => { return 'Konichiwa, whirlled world!';}
    , quotes: () => { return Object.values(quotes); } 
  }
  , Mutation: {
    addQuote: async ( parent, quote, context ) => {
      if(!context.user ){
        throw new AuthorizationError("You must be logged in to perform this action.")
      }
      return addQuote(quote);
    }
    
    , editQuote: async ( parent, { id, ...quote }, context ) => {
      if(!context.user ){
        throw new AuthorizationError("You must be logged in to perform this action.")
      }

      if(!quotes[id]){
        throw new Error("Quote does not exist");
      }

      quotes[id] = {
        ...quotes[id]
        , ...quote
      };

      return quotes[id];
    }
    
    , deleteQuote: async ( parent, { id }, context ) => {
      if(!context.user ){
        throw new AuthorizationError("You must be logged in to perform this action.")
      }
      const ok = Boolean( quotes[id] );
      delete quotes[id];

      // return { ok: ok };
      return { ok };
    }

    , login: async ( parent, { username, password } ) => {
      return ( {
        token: await getToken( {username, password } )
      } );
    }

  }
};

// =============================================
// Authorization Header Verification
// =============================================
const context = async ( props ) => {
  
  const {req} = props; // console.log( "await req", await req );

  // const [, token ] = await req.headers.authorization.split(" ")[1];
  const [, token ] = (await req.headers.authorization || "" ).split("Bearer ");

  if( await token !== undefined ){
    console.log( "token", await token );
  }

  return {
    user: await getUser( await getUserIdFromToken( await token ) )
  }

};

// =============================================
// GraphQL Server assignment
// =============================================
const server = new ApolloServer( {typeDefs,resolvers,context} );

// =============================================
// GraphQL Server listening and execution
// =============================================
  
server.listen().then( ( props ) => {

  // console.log( "props" , props );
  const { url } = props;
  // console.log( "url" , url );
  
  console.log( `\n Backend server has started at ${url} . \n`);
} ); 
