// =============================================
// LOCAL DEVELOPMENT ENV VARS SETUP
// process.env SETUP
// Establish the NODE_ENV toggle settings
// =============================================
if( process.env.NODE_ENV !== 'production' ){ 
  const dotenv = require('dotenv'); 
  dotenv.config();
}

const { OKTA_ORG_URL, OKTA_ISSUER, OKTA_CLIENT_ID, OKTA_CLIENT_SECRET, API_TOKEN_VALUE, OKTA_TOKEN_REQUEST_ENDPOINT = `${OKTA_ISSUER}/v1/token` } = process.env;

// =============================================
// boilerplate
// =============================================
const fetch = require("node-fetch");
const { AuthenticationError } = require("apollo-server");

// =============================================
// initial variables - credentials encoding
// =============================================
const data = [ OKTA_CLIENT_ID, OKTA_CLIENT_SECRET ].join(":");
const basicAuth = Buffer.from( data ).toString("base64");

const getToken = async ( { username, password } ) => {

  const fetchrequestobject = {
    method: "POST"
    , headers: {
      "authorization": `Basic ${basicAuth}`
      , "accept": "application/json"
      , "content-type": "application/x-www-form-urlencoded"
    }
    , body: new URLSearchParams( {
      username: username
      , password: password
      , grant_type: "password"
      , scope: "openid"
    } ).toString()
  };

  const response = await fetch( OKTA_TOKEN_REQUEST_ENDPOINT, fetchrequestobject );

  const { error_description, access_token } = await response.json();

  if( error_description ){
    console.log( "error_description" , error_description );
    throw new AuthenticationError( error_description );
  }
  console.log( "access_token" , access_token );
  return access_token;
};

// =============================================
// okta variables - jwt functionality
// =============================================
const OktaJwtVerifier = require("@okta/jwt-verifier");
const verifier = new OktaJwtVerifier( { issuer: OKTA_ISSUER , clientId: OKTA_CLIENT_ID } );

const getUserIdFromToken = async ( token ) => {
  if(!token){
    return;
  }
  try{
    console.log( "token", token ); 
    const jwt = await verifier.verifyAccessToken( token, "api://default" ); 
    const { claims } = await jwt; // console.log( "await claims", await claims );
    const { sub } = await claims; 
    console.log( "await sub", await sub );

    // return jwt.claims.sub;
    return sub;
  }
  catch(error){
    console.log( "error" , error );
  }
};

// =============================================
// okta variables - client functionality
// =============================================
const { Client } = require("@okta/okta-sdk-nodejs");
const client = new Client( { orgUrl: OKTA_ORG_URL , token: API_TOKEN_VALUE } );

const getUser = async ( userId ) => {
  if(!userId){
    return
  }
  try{
    console.log("userId" , userId );
    const user = await client.getUser( userId ); // console.log( "await user", await user );
    const { profile } = await user;
    console.log( "await profile", await profile );
    
    // return user.profile;
    return profile;
  }
  catch(error){
    console.log( "error" , error );
  }
};

module.exports = { getToken, getUserIdFromToken, getUser };