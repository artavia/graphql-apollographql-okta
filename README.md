# GraphQL, ApolloGraphQL &amp; Okta

## Description
This is another re&#45;hash of a project by Braden Kelley on the subjects of ApolloGraphQL and Okta found at the Okta blog.

### Processing and/or completion date(s)
  - July 30, 2020 &amp; August 2, 2020 

## Attribution(s)
The original blog article called [Build a CRUD App with Node.js and GraphQL](https://developer.okta.com/blog/2019/05/29/build-crud-nodejs-graphql "link to okta blog") can be found at the okta blog. 

## What is different with this version?
I decided to use the **modern and/or @latest versions** inevitably bringing about the **breaking changes** that are guaranteed to materialize. In essence, I am building on precedent and on previous knowledge that I have been fortunate enough to come away with and learn due to previous exercises.

## This is one of many baby steps to cure personal shortfalls in deciphering Sequelize errors
I have in recent weeks attempted to complete the [ApolloGraphQL fullstack tutorial](https://www.apollographql.com/docs/tutorial/introduction/ "link to tutorial") (or, **AFT** for short) on two different occasions. The authors **purported** that one does not need to know the other minor themes in order to complete the lesson. That would be nice if it were true, however, it was not meant to be so.

This exercise is a refresher on the subject of **GraphQL** and is a component of the warm&#45;up phase so I can finally ease back into the completion of the aforementioned exercise.

I thank you for your interest.

## Like Clouseau, it&rsquo;s all coming back!!!
[Ah, yes! Ah, yes&hellip;!!!](https://www.youtube.com/watch?v=CzycofSj7EM "link to youtube")

### God bless